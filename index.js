const getIP = require('external-ip')()
const freenomFactory = require('freenom-dns')

const EMAIL_ADDRESS = 'YOUR_EMAIL_HERE'
const PASSWORD = 'YOUR_PASSWORD_HERE'

const freenom = freenomFactory.init(EMAIL_ADDRESS, PASSWORD)

const UPDATE_EVERY_X_MINUTE = 1

let lastIp

let onGoing = 0
let hasFailed = false

const updateDomain = () => {
  getIP((err, myIp) => {
    try {

      if (onGoing) console.log(new Date(), 'You have', onGoing, 'ongoing DNS update')

      if (err) throw err

      if (!/^((\d{1,3}\.){3})(\d{1,3})$/.test(myIp)) {
        throw new Error('Invalid IPV4 address ' + myIp)
      }

      if (!hasFailed && typeof (lastIp) !== 'undefined' && myIp == lastIp) {
        console.log(new Date(), 'No change in your IP address since last check', lastIp)
        return
      }

      console.log(new Date(), 'Your IP address changed from', lastIp, 'to', myIp)

      lastIp = myIp

      const config = {
        user: {
          email: 'suditomi@pm.me',
          password: '+founT32in',
        },
        domains: [
          { name: 'example.tk', type: 'A', target: myIp },
          { name: 'www.example.tk', type: 'A', target: myIp },
        ]
      }

      config.domains.forEach(async domain => {
        ++onGoing
        try {
          await freenom.dns.setRecord(domain.name, domain.type, domain.target, domain.ttl || 300 /* minimum ttl = 300 */)
          console.log(new Date(), 'Successfully updated the domain', domain.name)
        } catch (err) {
          console.log(new Date(), 'Failed to update the domain', domain.name, '->', err)
          hasFailed = true
        } finally {
          --onGoing
        }
      })
    } catch (err) { console.error(err) }
  })
}

updateDomain()
setInterval(updateDomain, 1000 * 60 * UPDATE_EVERY_X_MINUTE)